<?php

// Register Custom Taxonomy
function dojo_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Dojo Categorias', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Dojo Categoría', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Dojo Categorias', 'text_domain' ),
		'all_items'                  => __( 'All Items', 'text_domain' ),
		'parent_item'                => __( 'Parent Item', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
		'new_item_name'              => __( 'Nueva categoría (dojo)', 'text_domain' ),
		'add_new_item'               => __( 'Agregar nueva categoría', 'text_domain' ),
		'edit_item'                  => __( 'Editar categoria', 'text_domain' ),
		'update_item'                => __( 'actualizar', 'text_domain' ),
		'view_item'                  => __( 'Ver item', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separar items con comas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Agregar o remover items', 'text_domain' ),
		'choose_from_most_used'      => __( 'Seleccionar las mas usadas', 'text_domain' ),
		'popular_items'              => __( 'Populares', 'text_domain' ),
		'search_items'               => __( 'Buscar Items', 'text_domain' ),
		'not_found'                  => __( 'No encontrado', 'text_domain' ),
		'no_terms'                   => __( 'No hay items', 'text_domain' ),
		'items_list'                 => __( 'Lista de items', 'text_domain' ),
		'items_list_navigation'      => __( 'lista de navegación', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'query_var'                  => 'dojo_taxonomy',
	);
	register_taxonomy( 'dojo_taxonomy', array( 'dojo' ), $args );

}
add_action( 'init', 'dojo_taxonomy', 0 );


// Register Custom Post Type
function dojo() {

	$labels = array(
		'name'                  => _x( 'Entradas', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Entrada', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Suzuki Dojo', 'text_domain' ),
		'name_admin_bar'        => __( 'Suzuki Dojo', 'text_domain' ),
		'archives'              => __( 'Item Archivos', 'text_domain' ),
		'attributes'            => __( 'Item Atributos', 'text_domain' ),
		'parent_item_colon'     => __( 'Padre Item:', 'text_domain' ),
		'all_items'             => __( 'Todos los Items', 'text_domain' ),
		'add_new_item'          => __( 'Agregar nuevo Item', 'text_domain' ),
		'add_new'               => __( 'Agregar nuevo', 'text_domain' ),
		'new_item'              => __( 'Nuevo item', 'text_domain' ),
		'edit_item'             => __( 'Editar Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'Ver Item', 'text_domain' ),
		'view_items'            => __( 'Ver Items', 'text_domain' ),
		'search_items'          => __( 'buscar Item', 'text_domain' ),
		'not_found'             => __( 'No se encuentra', 'text_domain' ),
		'not_found_in_trash'    => __( 'No se encontró en la papelera', 'text_domain' ),
		'featured_image'        => __( 'Imagen destacada', 'text_domain' ),
		'set_featured_image'    => __( 'Establecer imagen destacada', 'text_domain' ),
		'remove_featured_image' => __( 'Remover imagen destacada', 'text_domain' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'text_domain' ),
		'insert_into_item'      => __( 'Insertar dentro de item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Subido a este artículo', 'text_domain' ),
		'items_list'            => __( 'Lista de items', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Entrada', 'text_domain' ),
		'description'           => __( 'Contenido de Suzuki Dojo', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'revisions', 'thumbnail' ),
		'taxonomies'            => array( 'dojo_taxonomy' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'http://oqodigital.net/favicon-bk.png',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'dojo', $args );

}
add_action( 'init', 'dojo', 0 );
?>