<?php
/*
The Index Post (or excerpt)
===========================
Used by index.php, category.php and archive.php
*/
?>
<div class="col-sm-4">
  <article role="article" id="post_<?php the_ID()?>" <?php post_class("bk-loop-card mt-5"); ?> >
    <header class="bk-loop-card--header">
      <?php the_post_thumbnail(); ?>
    </header>
    <div class="bk-loop-card--content">
      <h2>
        <a href="<?php the_permalink(); ?>">
          <?php the_title()?>
        </a>
      </h2>
  
      <?php
      the_excerpt();
      ?>
    </div>
  </article>
</div>
