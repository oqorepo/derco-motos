    </main>
    <footer class="bk-footer">

        <hr class="bk-footer--hr__t">

        <div class="container">
            <div class="row align-items-end">
                <div class="col-sm">
                    <img src="<?php bloginfo('template_directory'); ?>/assets/img/bg-footer.jpg" alt="Suzuki way of life!" class="bk-footer__img">
                </div>
                <div class="col-sm">
                    <form>
                        <div class="input-parent">
                            <label for="exampleInputEmail1">Incríbete ahora</label>
                            <input class="effect-3" type="text" placeholder="Ingresa tu email">
                            <span class="focus-border"></span>
                        </div>
                    </form>
                </div>
                <div class="col-sm">
                    <div class="bk-footer--logos text-center">
                        <a href="https://twitter.com/suzukimotoscl"><img src="<?php bloginfo('template_directory'); ?>/assets/img/bk-twitter.png" alt=""></a>
                        <a href="https://www.facebook.com/SuzukiMotosChile/"><img src="<?php bloginfo('template_directory'); ?>/assets/img/bk-facebook.png" alt=""></a>
                        <a href="https://www.instagram.com/suzukimotos"><img src="<?php bloginfo('template_directory'); ?>/assets/img/bk-instagram.png" alt=""></a>
                        <!-- <a href="#"><img src="<?php bloginfo('template_directory'); ?>/assets/img/bk-linkedin.png" alt=""></a> -->
                        <a href="http://www.dercomotos.cl/"><img src="<?php bloginfo('template_directory'); ?>/assets/img/bk-derco.png" alt=""></a>
                    </div>  
                </div>
            </div>
        </div>

<!--         <?php if(is_active_sidebar('footer-widget-area')): ?>
        <div class="row border-bottom pt-5 pb-4" id="footer" role="navigation">
          <?php dynamic_sidebar('footer-widget-area'); ?>
        </div>
        <?php endif; ?> -->

        <hr class="bk-footer--hr__b">
        
        <div class="container-fluid bk-footer--copy">
            <div class="row">
                <div class="col ">
                    <div class="text-center pt-2 pb-2">
                        <span class="text-center"><a href="<?php echo home_url('/'); ?>" target="_blank" class="bk-link--white">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></a></span>
                    </div>
                </div>
            </div>
        </div>

    </footer>
    <?php wp_footer(); ?>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Inscríbete ahora</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body container">
                        <?php echo do_shortcode( '[contact-form-7 id="124" title="Formulario de contacto 1"]' ); ?>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
  </body>
</html>