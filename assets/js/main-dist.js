$(document).ready(function () {

    //Función que remueve el precargador una vez cargados todos los elementos de la pagina
    $(window).on('load', function () {
        $('.cd-loader').fadeOut('slow', function () {
            $(this).remove();
        });
    });


    $('.bk-navbar__nav .nav-item').addClass('d-flex align-self-stretch');
    $('.bk-navbar__nav .nav-link').addClass('align-self-center');

    // Agregar logo a 3ra posicion del menu principal
    $('.navbar-nav')
    .find(' > li:nth-last-child(2)')
    .before('<li class="text-center" id="bk-logo"><h1 class="text-center"><a href="' + my_data.templateUrl + '"><img src="' + my_data.template_directory_uri + '/assets/img/bk-hero-logo.png" alt="Suzuki boken" class="bk-navbar__img"></a></h1></li>');

    $('.bk-home--events__cards:first-child').removeClass('col-sm-4').addClass('col-sm-12 bk-home--events__first');

    $('.bk--telefono input').addClass('form-control');

    $('#nivel_conduccion_field').append('<h3 class="pt-4">Moto</h3>');
    $('#info_moto_kil_field').append('<h3 class="pt-4">Emergencia</h3>');


    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });

    $(window).load(function(){
		$(".col-3 input").val("");
		
		$(".input-effect input").focusout(function(){
			if($(this).val() != ""){
				$(this).addClass("has-content");
			}else{
				$(this).removeClass("has-content");
			}
		})
    });
    
    $('.hamburger').on('click', function () {
        $(this).toggleClass('is-active');
    });

    
});
