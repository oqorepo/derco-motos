<?php 
/*
Template Name: Sidebar
*/
?>
<?php get_template_part('includes/header'); ?>

<section class="container mt-5">
  <div class="row">

    <div class="col-sm">
      <div id="content" role="main">
        <?php get_template_part('includes/loops/page-content'); ?>
      </div><!-- /#content -->
    </div>

    <?php get_template_part('includes/sidebar'); ?>

  </div><!-- /.row -->
</section><!-- /.container -->

<?php get_template_part('includes/footer'); ?>
