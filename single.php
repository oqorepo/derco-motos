<?php get_template_part('includes/header'); ?>
<section class="container-fluid bk-single--img text-center">
<?php the_post_thumbnail(); ?>
</section>
  <?php get_template_part('includes/loops/single-post', get_post_format()); ?>

<?php get_template_part('includes/footer'); ?>
<?php get_template_part('includes/map'); ?>